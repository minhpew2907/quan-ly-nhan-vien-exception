﻿using ClassStudentException;

EmployeeManagement employeeManagement = new EmployeeManagement();

int choice = 0;
do
{
    try
    {
        employeeManagement.Menu();
        choice = int.Parse(Console.ReadLine());
        Console.Clear();

        switch (choice)
        {
            case 1:
                employeeManagement.View();
                break;
            case 2:
                employeeManagement.Add();
                break;
            case 3:
                employeeManagement.Remove();
                break;
            case 4:
                Console.WriteLine("Hen gap lai");
                Environment.Exit(0);
                break;
            default:
                Console.WriteLine("Vui long nhap so theo bang tren.");
                break;
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 4);