﻿
namespace ClassStudentException
{
    public class EmployeeManagement
    {
        private Employee[] employees = new Employee[100];

        public void Add()
        {
            try
            {
                Employee employee = new Employee();

                Console.WriteLine("Nhap ten nhan vien: ");
                employee.Name = Console.ReadLine();
                Console.WriteLine("Nhap tuoi nhan vien: ");
                employee.Age = int.Parse(Console.ReadLine());
                Console.WriteLine("Nhap vi tri nhan vien: ");
                employee.Position = Console.ReadLine();
                Console.WriteLine("Nhap luong nhan vien: ");
                employee.Salary = double.Parse(Console.ReadLine());

                for (int i = 0; i < employees.Length; i++)
                {
                    if (employees[i] is null)
                    {
                        employees[i] = employee;
                        Console.WriteLine("Them nhan vien thanh cong");
                        break;
                    }
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Nhap sai dinh dang, vui long nhap lai");
            }
        }

        public void Remove()
        {
            Console.WriteLine("Nhap ten nhan vien can xoa");
            string name = Console.ReadLine();
            for (int i = 0; i < employees.Length; i++)
            {
                if (employees[i] is not null && employees[i].Name == name)
                {
                    employees[i] = null;
                    Console.WriteLine($"Nhan vien {name} da duoc xoa khoi danh sach");
                    break;
                }
            }
        }

        public void View()
        {
            Console.Write("----Thong tin nhan vien----");
            foreach (Employee emp in employees)
            {
                if (emp is not null)
                {
                    Console.WriteLine($"\nTen nhan vien: {emp.Name}" +
                                      $"\nTuoi nhan vien: {emp.Age}" +
                                      $"\nVi tri nhan vien: {emp.Position}" +
                                      $"\nLuong nhan vien: {emp.Salary}");
                }
            }
        }

        public void Menu()
        {
            Console.WriteLine("\n----Quan li nhan vien----");
            Console.WriteLine("1. Hien thi thong tin nhan vien");
            Console.WriteLine("2. Them nhan vien moi");
            Console.WriteLine("3. Xoa nhan vien");
            Console.WriteLine("4. Thoat");
            Console.WriteLine("-------------------------");
            Console.WriteLine("Nhap lua chon cua ban:");
        }
    }
}
